import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from pyspark.sql import functions as F
from pyspark.sql.functions import col
from pyspark.sql.types import *
from pyspark.sql import SQLContext
from pyspark.sql import SparkSession

spark = SparkSession.builder.appName("consistency").getOrCreate()

print(spark.version)

# for pyspark
sc = spark.sparkContext
sqlContext = SQLContext(sc)


schema = StructType([
    StructField("rse", StringType(), True),
    StructField("scope", StringType(), True),
    StructField("name", StringType(), True),
    StructField("account", StringType(), True),
    StructField("bytes", IntegerType(), True),
    StructField("created_at", StringType(), True),
    StructField("accessed_at", StringType(), True),
    StructField("rule_id", StringType(), True),
    StructField("dataset_replicas", IntegerType(), True),
    StructField("updated_at", StringType(), True)])


consistency_datasets = spark.read.csv(
    '/user/rucio01/reports/2020-10-14/consistency_datasets/UTA_SWT2_DATADISK/part-00030-3a746c2b-4b91-4f5d-a594-aeb37848e5a7.c000.csv', sep='\t', header=False, schema=schema).dropna()

consistency_datasets = spark.read.csv(
    '/user/rucio01/reports/2020-10-14/consistency_datasets/*/*[0-9]',
    sep='\t', header=False, schema=schema).dropna()
consistency_datasets.createOrReplaceTempView("consistency_datasets")
pd = consistency_datasets.filter('rse ="CERN-PROD_SCRATCHDISK"').toPandas()
pd.groupby('account')['bytes'].sum().div(1000**3)
print(pd[['account', 'bytes']].head(20))


# read in datasets per RSE
schema = StructType([
    StructField("rse", StringType(), True),
    StructField("scope", StringType(), True),
    StructField("name", StringType(), True),
    StructField("account", StringType(), True),
    StructField("bytes", IntegerType(), True),
    StructField("created_at", StringType(), True),
    StructField("updated_at", StringType(), True),
    StructField("accessed_at", StringType(), True),
    StructField("rule_id", StringType(), True),
    StructField("state", StringType(), True)])

datasets_per_rse = spark.read.csv(
    '/user/rucio01/reports/2019-08-13/datasets_per_rse/*/*[0-9]', sep='\t', header=False, schema=schema)
datasets_per_rse.createOrReplaceTempView("datasets_per_rse")


# or

schema = StructType([
    StructField("rse", StringType(), True),
    StructField("scope", StringType(), True),
    StructField("name", StringType(), True),
    StructField("checksum", StringType(), True),
    StructField("bytes", IntegerType(), True),
    StructField("created_at", StringType(), True),
    StructField("path", StringType(), True),
    StructField("updated_at", StringType(), True),
    StructField("state", StringType(), True),
    StructField("accessed_at", StringType(), True),
    StructField("tombstone", StringType(), True)])

replicas_per_rse = spark.read.csv(
    '/user/rucio01/reports/2019-08-13/*/*', sep='\t', header=False, schema=schema)
replicas_per_rse.createOrReplaceTempView("replicas_per_rse")


#
sqlContext.sql("SELECT rse, sum(bytes) FROM replicas_per_rse GROUP BY rse ORDER BY sum(bytes) DESC").show(
    50, truncate=False)

#
consistency_datasets.dropna().groupby('rse').agg(
    F.sum(consistency_datasets.bytes)).show(50, truncate=False)

#
sqlContext.sql('SELECT rse, sum(bytes) FROM datasets_per_rse GROUP BY rse ORDER BY sum(bytes) DESC').show(
    50, truncate=False)
