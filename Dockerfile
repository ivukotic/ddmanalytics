FROM gitlab-registry.cern.ch/db/cerndb-infra-hadoop-conf:testing-hdp3

LABEL maintainer Ilija Vukotic <ivukotic@cern.ch>

# for newer python
# RUN yum -y update
RUN yum -y groupinstall "Development Tools"
RUN yum -y install openssl-devel bzip2-devel libffi-devel wget curl


RUN pip3 install --upgrade pip

RUN python3 -m pip --no-cache-dir install \
    requests \
    matplotlib \
    numpy \
    pandas \
    Pillow \
    scipy \
    sklearn \
    elasticsearch
# qtpy \
# tensorflow \
# tqdm \
# graphviz \
# JSAnimation \
# ipywidgets \
# Cython

COPY *.py /
COPY *.sh /

# build info
RUN echo "Timestamp:" `date --utc` | tee /image-build-info.txt

ARG UNAME=analyticssvc
ARG UID=110942
ARG GID=2766
RUN groupadd -g $GID $UNAME
RUN useradd -m -u $UID -g $GID -s /bin/bash $UNAME
USER $UNAME

CMD [ "sleep","9999999" ]