#!/bin/bash
echo "  *******************************  ddm collector   *******************************"

kinit analyticssvc@CERN.CH -k -t /tmp/keytab/analyticssvc.keytab
klist
source hadoop-setconf.sh analytix 3.2 spark3

/usr/hdp/spark3/bin/spark-submit --master yarn --deploy-mode cluster /usr/hdp/spark3/examples/src/main/python/pi.py

/usr/hdp/spark3/bin/spark-submit --master yarn --deploy-mode cluster test.py  

/usr/hdp/spark3/bin/spark-submit --master yarn --deploy-mode cluster consistency.py 

/usr/hdp/spark3/bin/spark-submit --deploy-mode client test-local.py

rc=$?; if [[ $rc != 0 ]]; then 
    echo "problem with ddm indexer. Exiting."
    exit $rc
fi

echo "Indexing DONE."