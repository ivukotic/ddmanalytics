import pyspark
from pyspark.sql.types import *
from pyspark.sql import SparkSession
from pyspark import SparkContext

sc = SparkContext("local", "testDDM")
print(sc)

spark = SparkSession\
    .builder\
    .appName("testDDM")\
    .getOrCreate()

print(spark.version)
# sc = spark.sparkContext

schema = StructType([
    StructField("rse", StringType(), True),
    StructField("scope", StringType(), True),
    StructField("name", StringType(), True),
    StructField("account", StringType(), True),
    StructField("bytes", IntegerType(), True),
    StructField("created_at", StringType(), True),
    StructField("accessed_at", StringType(), True),
    StructField("rule_id", StringType(), True),
    StructField("dataset_replicas", IntegerType(), True),
    StructField("updated_at", StringType(), True)])

spark.read.csv('hdfs://analytix/user/rucio01/reports/2020-10-14/consistency_datasets/UTA_SWT2_DATADISK/part-00030-3a746c2b-4b91-4f5d-a594-aeb37848e5a7.c000.csv',
               sep='\t', header=False, schema=schema).show(3)
